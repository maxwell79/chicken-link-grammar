;;;                        ____ _     _      _              
;;;                       / ___| |__ (_) ___| | _____ _ __  
;;;                      | |   | '_ \| |/ __| |/ / _ \ '_ \ 
;;;                      | |___| | | | | (__|   <  __/ | | |
;;;                       \____|_| |_|_|\___|_|\_\___|_| |_|
;;;                                                         
;;;      _     _       _       ____                                          
;;;     | |   (_)_ __ | | __  / ___|_ __ __ _ _ __ ___  _ __ ___   __ _ _ __ 
;;;     | |   | | '_ \| |/ / | |  _| '__/ _` | '_ ` _ \| '_ ` _ \ / _` | '__|
;;;     | |___| | | | |   <  | |_| | | | (_| | | | | | | | | | | | (_| | |   
;;;     |_____|_|_| |_|_|\_\  \____|_|  \__,_|_| |_| |_|_| |_| |_|\__,_|_|   
;;;
;;;                     ____  _           _ _
;;;                    | __ )(_)_ __   __| (_)_ __   __ _ ___
;;;                    |  _ \| | '_ \ / _` | | '_ \ / _` / __|
;;;                    | |_) | | | | | (_| | | | | | (_| \__ \
;;;                    |____/|_|_| |_|\__,_|_|_| |_|\__, |___/
;;;                                                 |___/

;;;
;;; chicken-link-grammar: Scheme bindings to the link grammar parsing system
;;;
;;; Copyright © 2018  David Ireland. 
;;; All rights reserved. 
;;;
;;; | --------------------------------------------------------------------- |
;;; | Use of the link grammar parsing system is subject to the terms of the | 
;;; | license set forth in the LICENSE file included with this software.    |
;;; | This license allows free redistribution and use in source and binary  |
;;; | forms, with or without modification, subject to certain conditions.   |
;;; | --------------------------------------------------------------------- |                                                                       

(module link-grammar 
    (parse-with-default
     parse-sentence 
     display-off 
     display-multi-line
     display-bracket-tree
     display-single-line
     display-max-styles 
     create-default-dictionary 
     create-dictionary-with-language 
     get-verbosity 
     get-version
     get-dictionary-version
     get-dictionary-locale
     get-dictionary-language  
     get-dictionary-data-dir 
     set-dictionary-data-dir!  
     delete-dictionary!  
     create-sentence
     split-sentence 
     sentence-length 
     sentence-null-count 
     sentence-disjunct-cost 
     sentence-link-cost
     linkages-found 
     linkages-post-processed 
     linkages-violated 
     valid-linkages 
     delete-sentence!
     create-linkage 
     corpus-cost 
     get-lword  
     get-rword  
     get-words
     get-word  
     get-constituents 
     get-diagram  
     get-postscript
     get-disjuncts 
     get-links-domains
     get-violation-name 
     link-length
     link-label
     link-llabel
     link-rlabel
     link-cost 
     link-domain-names 
     num-words
     num-links 
     num-domains
     unused-word-cost
     delete-linkage!
     init-opts 
     set-max-parse-time!
     set-linkage-limit! 
     set-short-length! 
     set-disjunct-cost! 
     set-min-null-count! 
     set-max-null-count! 
     set-max-parse-time! 
     set-islands-ok! 
     set-verbosity! 
     resources-exhausted? 
     memory-exhausted? 
     timer-expired? 
     reset-resources!
     delete-parse-options!)

  (import scheme)
  (cond-expand
    (chicken-4
     (import foreign chicken scheme)
     (use extras
	  srfi-1
	  s
	  loops))
    (chicken-5
     (import
       scheme
       srfi-1
       chicken.format
       chicken.base
       chicken.string
       chicken.foreign)))
 
  
  
  (foreign-declare "#include \"link-grammar/link-includes.h\"")
  (foreign-declare "#include \"link-grammar/link-features.h\"")
  
;;; Link Grammar 5.4.4 doesn't compile when including these header files
;;;(foreign-declare "#include \"link-grammar/dict-api.h\"")
;;;(foreign-declare "#include \"link-grammar/dict-structures.h\"")

  (include "link-grammar-core.scm"))


