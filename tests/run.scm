(use test
     (srfi 1 4) 
     link-grammar)

(test-begin "parsing")
(let* ((text  "The black fox ran from the the hunters")
       (num   (length (parse-with-default text))))
  (test "black-fox" num 4))
(test-end "parsing")
(test-exit)


