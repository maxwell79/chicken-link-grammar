(import scheme)
(cond-expand
  (chicken-4
   (use (prefix link-grammar lg:)))
  (chicken-5
   (import
     (prefix link-grammar lg:))))
   

(define (display-linkage sentence opts index)
  (let* ([links-found (lg:linkages-found sentence)]
         [linkage (lg:create-linkage index sentence opts)])
    (when linkage
      (let ([constituents (lg:get-constituents linkage lg:display-multi-line)]
            [diagram      (lg:get-diagram linkage #t 80)])
        (print constituents)
        (print diagram)
        (lg:delete-linkage! linkage)))
  (when (<= index links-found)
    (display-linkage sentence opts (+ index 1)))))

(define (parse text dictionary opts)
  (let* ([sentence     (lg:create-sentence text dictionary)]
         [num-linkages (lg:parse-sentence sentence opts)])
    (when (= num-linkages 0)
      (lg:set-min-null-count! opts  1)
      (lg:set-max-null-count! opts (lg:sentence-length sentence))
      (set! num-linkages (lg:parse-sentence sentence opts)))
      (display-linkage sentence opts 0)
      (lg:delete-sentence! sentence)))

; Main Entry
(define dictionary (lg:create-default-dictionary))
(define opts       (lg:init-opts))
(lg:set-linkage-limit!  opts 1000)
(lg:set-short-length!   opts 10)
(lg:set-verbosity!      opts 1)
(lg:set-max-parse-time! opts 30)
(lg:set-linkage-limit!  opts 1000)
(lg:set-min-null-count! opts 0)
(lg:set-max-null-count! opts 0)
(lg:set-short-length!   opts 16)
(lg:set-islands-ok!     opts #f)

(parse "The black fox ran from the hunters" dictionary opts)
(lg:delete-parse-options! opts)
(lg:delete-dictionary! dictionary)

