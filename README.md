# chicken-link-grammar


CHICKEN Scheme bindings to  [CMU link-grammar parsing system](http://www.link.cs.cmu.edu/link/) a.k.a. the link-grammar egg.

* Version:     1.6 (2018-15-11)
* API Docs:    [Chicken4](https://wiki.call-cc.org/eggref/4/link-grammar)
* API Docs:    [Chicken5](https://wiki.call-cc.org/eggref/5/link-grammar)
* License:     LGPL v2.1
* Maintainer:  David Ireland (djireland79 at gmail dot com)
* Installation: chicken-install link-grammar
* Dependencies: 
    * [link-grammar](http://www.link.cs.cmu.edu/link/) 5.3 or higher
