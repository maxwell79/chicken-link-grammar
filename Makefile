default: build

wiki:
	hahn -o link-grammar.wiki link-grammar.scm link-grammar-core.scm
build:
	chicken-install -n 

install:
	chicken-install

install-test:
	chicken-install -test

uninstall:
	chicken-uninstall link-grammar

test:
	cd tests && csi -s run.scm

clean:
	rm -f link-grammar*.sh link-grammar*.link  *.import.scm *.log *.types *.c *.o *.so ./examples/example
